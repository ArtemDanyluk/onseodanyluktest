﻿using UnityEngine;

public class WaitingPlayerPrefs : CustomYieldInstruction
{
    private string response;

    public WaitingPlayerPrefs(string message)
    {
        response = message;

       // Debug.Log("WaitingPlayerPrefs");
    }

    public override bool keepWaiting
    {
        get
        {
            // Debug.Log("keepWaiting");
            return response != null ;
        }
    }

    
}
