﻿using UnityEngine;
using Net;
using ResourcesSenders;
using Authorizations;

namespace Root
{
    public class Main : MonoBehaviour, IMain
    {
        public IServer Server { get; set; }
        public IResourcesSender ResourcesSender { get; set; }
        public IAuthorization Authorization { get; set; }


        void Awake()
        {
            Server = new Server();
            Authorization = new Authorization(this);
            new Test.TestData(this);
            ResourcesSender = new ResourcesSender(this);
        }



    }
}