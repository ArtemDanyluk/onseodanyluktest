﻿using Net;
using ResourcesSenders;

namespace Root
{
    public interface IMain
    {
        IServer Server { get; set; }
        IResourcesSender ResourcesSender { get; set; }
    }
}
