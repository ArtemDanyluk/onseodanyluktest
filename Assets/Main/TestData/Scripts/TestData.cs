﻿using GameData;
using GameResources;
using Root;
using System.Collections.Generic;

namespace Test
{
    public class TestData
    {
        private readonly IMain main;

        public TestData(IMain obj)
        {
            main = obj;
            TestFillResources();
        }

        private void TestFillResources()
        {
            var resourcesList = new ResourcesList();
            resourcesList.AllGameResources = new List<Resource>() {
                                                                new Resource() { ID = 0, Name = "Gold" },
                                                                new Resource() { ID = 1, Name = "Wood" },
                                                                new Resource() { ID = 2, Name = "Coal" } };

            main.Server.Set(Identifiers.ResourcesListID, resourcesList);
        }
    }
}