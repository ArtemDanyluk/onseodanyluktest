﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Net
{
    public class Server : IServer
    {
        private bool networkConnection;
        private IServer server;

        public Server()
        {
            if (networkConnection)
            {
                server = new WebServer();
            }
            else
            {
                server = new GameObject("LocalServer").AddComponent<LocalServer>();
            }  
        }

        public void Set(string ID, object obj)
        {
            server.Set(ID, obj);
        }

        public void Get(string ID, Action<object, string> action)
        {
            server.Get(ID, action);
        }
    }
}