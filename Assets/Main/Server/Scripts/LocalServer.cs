﻿using System;
using UnityEngine;
using System.Collections;

namespace Net
{
    public class LocalServer : MonoBehaviour, IServer
    {

        public void Get(string ID, Action<object, string> action)
        {
            if (String.IsNullOrEmpty(ID))
            {
                action(null, "ID is Empty");
                return;
            }
            StartCoroutine(GetValue(ID, action));
        }

        IEnumerator GetValue(string ID, Action<object, string> action)
        {
            string response;
            response = PlayerPrefs.GetString(ID);
            Debug.Log(response);

            yield return new WaitingPlayerPrefs(response);
            var valueObject = JsonUtility.FromJson<object>(response);
            action(valueObject, null);
        }

        public void Set(string ID, object obj)
        {
            var jsonValue = JsonUtility.ToJson(obj);
            PlayerPrefs.SetString(ID, jsonValue);
        }
    }
}

