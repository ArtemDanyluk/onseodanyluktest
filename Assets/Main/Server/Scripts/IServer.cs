﻿using System;

namespace Net
{
    public interface IServer
    {
        /// <summary>
        /// object - saved object, string - errorMessage
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="action"></param>
        void Get(string ID, Action<object, string> action);
        void Set(string ID, object obj);
    }
}
