﻿namespace GameData
{
   public class Identifiers
    {
        public const string ResourcesListID = "ResourcesList";
        public const string UsersListID = "UsersList";
    }
}
