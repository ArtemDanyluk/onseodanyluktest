﻿using UnityEngine;
using Root;
using GameResources;
using System.Collections.Generic;
using GameData;

namespace ResourcesSenders
{
    public class ResourcesSender : IResourcesSender
    {
        private readonly IMain main;

        private List<Resource> existingResourcesTypes;

        public ResourcesSender(IMain obj)
        {
           
            main = obj;
            GetExistingResourcesTypes();
        }

        public void Set()
        {
            main.Server.Get(Identifiers.ResourcesListID, OnGetTypes);
        }


        private void GetExistingResourcesTypes()
        {
            main.Server.Get(Identifiers.ResourcesListID, OnGetTypes);
        }

        private void OnGetTypes(object obj, string errorMessage)
        {
            ResourcesList resourcesList = (ResourcesList)obj;
            existingResourcesTypes = resourcesList.AllGameResources;
            Debug.Log(existingResourcesTypes.Count);
        }
    }
}