﻿namespace Authorizations
{
   public interface IAuthorization
    {
        string UserID { get; }
        void OnLogin();
    }
}
