﻿using GameData;
using Root;
using UnityEngine;
using System.Collections.Generic;

namespace Authorizations
{
   public class Authorization : IAuthorization
    {
        public string UserID { get; private set; }

        private readonly IMain main;


        public Authorization(IMain obj)
        {
            main = obj;
            CreateUI();
        }

        public void SetUser(string userID)
        {
            var usersList = new UsersList();
            usersList.AllUsers = new List<User>() { new User() { ID = userID } };

            main.Server.Set(Identifiers.UsersListID, usersList);
        }

        public void OnLogin()
        {
            main.Server.Get(Identifiers.UsersListID, OnGetUser);
        }

        private void OnGetUser(object obj, string errorMessage)
        {
            UsersList usersList = (UsersList)obj;
           List<User> existingUsers = usersList.AllUsers;
        }

        private void CreateUI()
        {
            var UIPrefab = Resources.Load<GameObject>("Authorizations/AuthorizationPopUp");
            var UIPopUp = GameObject.Instantiate(UIPrefab);
            UIPopUp.GetComponent<UI>().Authorization = this;
        }


    }
}
