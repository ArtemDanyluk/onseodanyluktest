﻿using UnityEngine;
using UnityEngine.UI;

namespace Authorizations
{
    public class UI : MonoBehaviour
    {
        public IAuthorization Authorization;

        [SerializeField]
        private InputField inputField;

        public void OnClickLogin()
        {
          var inputFieldText = inputField.GetComponent<Text>().text;
            Authorization.OnLogin();
        }

    }
}
