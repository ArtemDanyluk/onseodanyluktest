﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameResources
{
    [Serializable]
    public class Resource
    {
        public int ID;
        public string Name;
        public int Count;
        public string ImageURL;
    }
}
