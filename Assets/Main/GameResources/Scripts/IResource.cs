﻿namespace GameResources
{
    public interface IResource
    {
        int ID { get; set; }
        string Name { get; set; }
        int Count { get; set; }
        string ImageURL { get; set; }
    }
}